export class NewsMode {
    id: number;
    date : String;
    news_no : String;
    subject : String;
    firstname : String;
    middlename : String;
    lastname : String; 
    no : String;
    street : String;
    barangay : String;
    municipality : String;
    province : String;
    zipcode : String;
    descrip : String;
    status : String;
}
