import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LeaveserviceService } from 'src/app/service/leaveservice.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-leave-form',
  templateUrl: './leave-form.component.html',
  styleUrls: ['./leave-form.component.css']
})
export class LeaveFormComponent implements OnInit {
  // , private newservice: LeaveserviceService, private toastr: ToastrService 
  formleave: FormGroup;

  constructor(private leave: FormBuilder, private leaveservice: LeaveserviceService,  private toastr: ToastrService ) { }

  ngOnInit() {

    this.formleave = this.leave.group({

      ltno : ['', Validators.required],
      ldfrom : ['', Validators.required],
      ldto : ['', Validators.required],
      reason : ['', Validators.required],
      turnover : ['', Validators.required],
      remarks : ['', Validators.required],
      letterno : ['', Validators.required]
      
    });

  } // 

  get ltno() { return this.formleave.get('ltno'); }
  get ldfrom() { return this.formleave.get('ldfrom'); }
  get ldto() { return this.formleave.get('ldto'); }
  get reason() { return this.formleave.get('reason'); }
  get turnover() { return this.formleave.get('turnover'); }
  get remarks() { return this.formleave.get('remarks'); }
  get letterno() { return this.formleave.get('letterno'); }


  submitleave(){

    this.leaveservice.create(this.formleave.value)
      .subscribe(
        data => {// reset form
          this.formleave.reset();
          this.toastr.success("Successfully Added!")
          console.log('success', data)
        },
        error => console.error('Error', error)
      )

      }


}

