import { Component, OnInit } from '@angular/core';
import { NewsServiceService } from 'src/app/service/news-service.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { NewsInterface } from 'src/app/interface/news-interface';


export class NewsMode {
  id: number;
  date : String;
  news_no : String;
  subject : String;
  descrip : String;
 
}


@Component({
  selector: 'app-detailsnews',
  templateUrl: './detailsnews.component.html',
  styleUrls: ['./detailsnews.component.css']
})


export class DetailsnewsComponent implements OnInit {
  dataSource: MatTableDataSource<NewsInterface>;
  // dataSource = [] ;
  news: any = [];
  newsform: FormGroup;
  news_details: any = [];
  submitted = false; // submited deafault to false
  // 
  constructor(private news_service: NewsServiceService,
              private toastr: ToastrService,
              private  router: Router,
              private formbuilder: FormBuilder,) { }

  ngOnInit() {

    this.reloaddata();

    
    this.newsform = this.formbuilder.group({
      date : ['', Validators.required],
      news_no : ['', [Validators.required,Validators.pattern('^[0-9]+$')]],
      subject : ['', Validators.required],
      descrip : ['', Validators.required]
    });


  }
  
  

  reloaddata(){
    this.news_service.getdata().pipe(first())
    .subscribe(data => { 
      this.news = data
      // this.dataSource = data.body;
      this.dataSource = new MatTableDataSource<NewsInterface>(this.news);
      this.dataSource.data = this.news;
    });
  }

  

  // 
  displayedColumns: string[] = ['id', 'date', 'news_no', 'subject', 'descrip'];
  
  //
 


   delete(id: number) {
    this.news_service.destroynews(id).pipe(first()).subscribe(data => {
          console.log(data);
          this.toastr.success("Successfully deleted")
          this.reloaddata();
          this.router.navigate['/newsdetails']
    });
    
  }


  edit(id: number) {
    this.news_service.getspecific(id).subscribe(data => {
      console.log(data);
      this.news_details =  data
      this.newsform = this.formbuilder.group({
        date : [this.news_details.date, Validators.required],
      news_no : [this.news_details.news_no, [Validators.required,Validators.pattern('^[0-9]+$')]],
      subject : [this.news_details.subject, Validators.required],
      descrip : [this.news_details.descrip, Validators.required]
       
      });
    });
      
       
   }

   get date() { return this.newsform.get('date'); }
   get news_no() { return this.newsform.get('news_no'); }
   get subject() { return this.newsform.get('subject'); }
   get descrip() { return this.newsform.get('descrip'); }


   get f() { return this.newsform.controls; }
  

   submitForm(news_details){
     this.submitted = true;
     if (this.newsform.invalid) {
       return;
     }else{
       // $("#exampleModal").modal("hide");
       alert('Sucess');
      //  location.reload(true);
     }
     // console.log(id)
     // console.log(this.f.fname.value);
     // return false;
     const data = {
       
       id : news_details.id,
       date: this.f.date.value,
       news_no: this.f.news_no.value,
       subject: this.f.subject.value,
       descrip: this.f.descrip.value
      
 
     }
    
     this.news_service.updatenow(data)
     .subscribe(data => {
       this.reloaddata();
       this.router.navigate['/newsdetails']
       console.log(data);
     })
     // console.log(this.formreact.value)

 }  

 
}
