import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA} from '@angular/material/dialog';
import { LeaveserviceService } from 'src/app/service/leaveservice.service';

@Component({
  selector: 'app-mydialog',
  templateUrl: './mydialog.component.html',
  styleUrls: ['./mydialog.component.css']
})
export class MydialogComponent implements OnInit {

  formleave: FormGroup;
  // leave_details: any = [];
  
  
  constructor(private leave: FormBuilder, private leaveservice: LeaveserviceService, 
    public dialogRef: MatDialogRef<MydialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    
  
    this.formleave = this.leave.group({    //print my data to the dialog

      ltno : [this.data.ltno, Validators.required],
      ldfrom : [this.data.ldfrom, Validators.required],
      ldto : [this.data.ldto, Validators.required],
      reason : [this.data.reason, Validators.required],
      turnover : [this.data.turnover, Validators.required],
      remarks : [this.data.remarks, Validators.required],
      letterno : [this.data.letterno, Validators.required]
      
    });
    
      

  }


    get ltno() { return this.formleave.get('ltno'); }
    get ldfrom() { return this.formleave.get('ldfrom'); }
    get ldto() { return this.formleave.get('ldto'); }
    get reason() { return this.formleave.get('reason'); }
    get turnover() { return this.formleave.get('turnover'); }
    get remarks() { return this.formleave.get('remarks'); }
    get letterno() { return this.formleave.get('letterno'); }
  

}
