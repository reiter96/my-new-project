import { Component, OnInit } from '@angular/core';
import { LeaveserviceService } from 'src/app/service/leaveservice.service';
import { LeaveInterface } from 'src/app/interface/leave-interface';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-leavespecific',
  templateUrl: './leavespecific.component.html',
  styleUrls: ['./leavespecific.component.css']
})
export class LeavespecificComponent implements OnInit {


  details: LeaveInterface;
 
  constructor(private route: ActivatedRoute,
    public router: Router, 
    private leaveservice: LeaveserviceService) {}
// 
  ngOnInit() {

    // 

       const id = this.route.snapshot.params['id'];

       this.leaveservice.specificdata(id).subscribe(data =>{
          console.log(data);

        
          this.details  = data as LeaveInterface;
        
      })   
    




  }




}
