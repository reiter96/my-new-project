import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavespecificComponent } from './leavespecific.component';

describe('LeavespecificComponent', () => {
  let component: LeavespecificComponent;
  let fixture: ComponentFixture<LeavespecificComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavespecificComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavespecificComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
