import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LeaveInterface } from 'src/app/interface/leave-interface';
import { LeaveserviceService } from 'src/app/service/leaveservice.service';
import { first } from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MydialogComponent } from '../mydialog/mydialog.component';
import { Validators, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-leavedetails',
  templateUrl: './leavedetails.component.html',
  styleUrls: ['./leavedetails.component.css']
})
export class LeavedetailsComponent implements OnInit {

  dataSource: MatTableDataSource<LeaveInterface>;
  leave: any = [];
  
  // 

  constructor(private leaveservice: LeaveserviceService,
              public dialog: MatDialog,
              private toastr: ToastrService,
              private  router: Router,) { }

  ngOnInit() {

    this.reloaddata();
    
  }


  openDialog(id: number): void {
    
    this.leaveservice.specificdata(id).subscribe(data => {    //getting data  to the database
      console.log(data);
      this.leave =  data

      const dialogRef = this.dialog.open(MydialogComponent, {   // passsing data to material dialog
          data: this.leave
      });

      dialogRef.afterClosed().subscribe(result => {              //closing the dialog
        console.log('The dialog was closed');
        
      });


    });

  }

 
  
  reloaddata(){
    this.leaveservice.getleave().subscribe(data => { 
      this.leave = data
      // this.dataSource = data.body;
      this.dataSource = new MatTableDataSource<LeaveInterface>(this.leave);
      this.dataSource.data = this.leave;
    });
  }

  delete(id: number){
    this.leaveservice.delete(id).pipe(first()).subscribe(data => {    //getting data  to the database
      console.log(data);
      this.reloaddata();
      this.router.navigate['/leavedetails']
      this.toastr.success("Successfully deleted")
      

    });
   
  }  


  displayedColumns: string[] = ['id', 'ltno', 'ldfrom', 'ldto', 'reason', 'actions'];

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  

}
