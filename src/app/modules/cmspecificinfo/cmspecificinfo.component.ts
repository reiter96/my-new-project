import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CmanagementService } from 'src/app/interface/cmanagement-service';
import { UsertypeServiceService } from 'src/app/service/usertype-service.service';

@Component({
  selector: 'app-cmspecificinfo',
  templateUrl: './cmspecificinfo.component.html',
  styleUrls: ['./cmspecificinfo.component.css']
})
export class CmspecificinfoComponent implements OnInit {

  cmdetails: CmanagementService;
 
  constructor(private router: Router, 
              private cmservice: UsertypeServiceService, //
              private route: ActivatedRoute,) {}

  ngOnInit() {

    
    const id = this.route.snapshot.params['id'];

    this.cmservice.specificdata(id).subscribe(data =>{
       console.log(data);

     
       this.cmdetails  = data as CmanagementService;
     
   })   

  } //

}
