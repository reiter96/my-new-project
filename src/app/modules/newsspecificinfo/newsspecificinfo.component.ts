import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewsServiceService } from 'src/app/service/news-service.service';
import { NewsMode } from 'src/app/models/news-mode';

@Component({
  selector: 'app-newsspecificinfo',
  templateUrl: './newsspecificinfo.component.html',
  styleUrls: ['./newsspecificinfo.component.css']
})
export class NewsspecificinfoComponent implements OnInit {

  newsdetails: NewsMode;

  constructor(private route: ActivatedRoute,
    public router: Router, 
    private newsservice: NewsServiceService) {}

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
       this.newsservice.getspecific(id).subscribe(data =>{
          console.log(data);
            
          this.newsdetails  = data as NewsMode;
        
      })   
     

  }

}
