import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InfoserviceService } from 'src/app/service/infoservice.service';
import { Form } from 'src/app/models/form';

@Component({
  selector: 'app-specificinfo',
  templateUrl: './specificinfo.component.html',
  styleUrls: ['./specificinfo.component.css']
})
export class SpecificinfoComponent implements OnInit {
  
  details: Form;
  
  constructor(private route: ActivatedRoute,
             public router: Router, 
             private infoservice: InfoserviceService) {}

  ngOnInit() {

    const id = this.route.snapshot.params['id'];
    // this.route.paramMap.subscribe(params => {
    //   console.log(params.get('id'))
       this.infoservice.getspecific(id).subscribe(c =>{
          console.log(c);

        
          this.details  = c as Form;
        
      })   
      // });


  }

}
