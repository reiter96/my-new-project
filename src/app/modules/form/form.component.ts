import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormserviceService } from 'src/app/service/formservice.service';
import { Form } from 'src/app/models/form';



@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  reactForm: FormGroup;
  submitted = false; // submited deafault to false
  constructor(private _formbuilder: FormBuilder, private formservice: FormserviceService) { }

  ngOnInit() {
    this.reactForm = this._formbuilder.group({
      fname : ['', [ Validators.required, Validators.minLength(2)]],
      mname : ['', [ Validators.required, Validators.minLength(2)]],
      lname : ['', [ Validators.required, Validators.pattern('^[A-Za-z-ñÑáéíóúÁÉÍÓÚ ]+$')]],
      bday : ['', [ Validators.required, Validators.pattern('^[0-3][0-9]/[0-3][0-9]/(?:[0-9][0-9])?[0-9][0-9]$')]],
      gender : ['', Validators.required],
      password : ['', Validators.required],
      contact : ['', Validators.required],
      // password : ['', [ Validators.required, Validators.minLength(2)]],
      // contact : ['', [ Validators.required, Validators.minLength(2)]],
      // profile : ['', [ Validators.required, Validators.minLength(2)]]
    });

    

  } //ngonit

  get fname() { return this.reactForm.get('fname'); }
  get mname() { return this.reactForm.get('mname'); }
  get lname() { return this.reactForm.get('lname'); }
  get bday() { return this.reactForm.get('bday'); }
  get gender() { return this.reactForm.get('gender'); }
  get password() { return this.reactForm.get('password'); }
  get contact() { return this.reactForm.get('contact'); }

  submitForm(){
    this.submitted = true; 
    if (this.reactForm.invalid) {
      return;
    }else{
     
      alert('Sucess');
      
     
    }
    this.formservice.createform(this.reactForm.value)
    .subscribe(
      data => {// reset form
        this.reactForm.reset();
        console.log('success', data)
      },
      error => console.error('Error', error)
    )
   
    console.log(this.reactForm);
    // this.reactForm.reset();
  }
 

}

// [ngClass]="{ 'is-invalid': submitted &amp;&amp; registerForm.controls.firstName.errors }"
