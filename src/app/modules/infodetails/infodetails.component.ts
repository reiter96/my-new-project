import { Component, OnInit } from '@angular/core';
import { InfoserviceService } from 'src/app/service/infoservice.service';
import { Form } from 'src/app/models/form';
import { Observable } from "rxjs";
import { first } from "rxjs/operators";
import { Router, ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormserviceService } from 'src/app/service/formservice.service';
import { ToastrService } from 'ngx-toastr';
import { Userinterface } from 'src/app/interface/userinterface';
declare var $: any;


@Component({
  selector: 'app-infodetails',
  templateUrl: './infodetails.component.html',
  styleUrls: ['./infodetails.component.css']
})
export class InfodetailsComponent implements OnInit {
  

  
  public employees = [];  
  formreact: FormGroup;
  submitted = false; // submited deafault to false
  details: any = [];
  constructor(private  infoservice: InfoserviceService, private  router: Router,
     private _formbuilder: FormBuilder,  private formservice: FormserviceService, private toastr: ToastrService) { }

  ngOnInit() {

 
    this.reloaddata();
    
    this.formreact = this._formbuilder.group({
      fname : ['',  Validators.required],
      mname : ['',  Validators.required],
      lname : ['', [ Validators.required, Validators.pattern('^[A-Za-z]+$')]],
      bday : ['', [ Validators.required, Validators.pattern('^[0-3][0-9]/[0-3][0-9]/(?:[0-9][0-9])?[0-9][0-9]$')]],
      gender : ['', Validators.required],
      password : ['', Validators.required],
      contact : ['', Validators.required],
     
    });  

  }

  reloaddata(){
    
    this.infoservice.getdata().pipe(first())
    .subscribe(data => {
       this.employees = data 
      });
  }

  getcolor(fname){
    switch(fname){
      case 'ara':
      return 'red';
      case 'joseph':
      return 'blue';
      case 'joy':
        return '#00b894';
    }
    
 }

 delete(id: number) {
    this.infoservice.deleteEmployee(id).pipe(first()).subscribe(data => {
          console.log(data);
          this.toastr.success("Successfully deleted")
          this.reloaddata();
          this.router.navigate['/infodetails']
    });
    
  }

 
  edit(id: number) {
    this.infoservice.getspecific(id).subscribe(data => {
      console.log(data);
      this.details =  data
      this.formreact = this._formbuilder.group({
        fname : [this.details.fname,  Validators.required],
        mname : [this.details.mname,  Validators.required],
        lname : [this.details.lname, [ Validators.required, Validators.pattern('^[A-Za-z]+$')]],
        bday : [this.details.bday, [ Validators.required, Validators.pattern('^[0-3][0-9]/[0-3][0-9]/(?:[0-9][0-9])?[0-9][0-9]$')]],
        gender : [this.details.gender, Validators.required],
        password : [this.details.password, Validators.required],
        contact : [this.details.contact, Validators.required],
       
      });

    
    });
      
       
   }

  
  get fname() { return this.formreact.get('fname'); }
  get mname() { return this.formreact.get('mname'); }
  get lname() { return this.formreact.get('lname'); }
  get bday() { return this.formreact.get('bday'); }
  get gender() { return this.formreact.get('gender'); }
  get password() { return this.formreact.get('password'); }
  get contact() { return this.formreact.get('contact'); }

 
  get f() { return this.formreact.controls; }
  

  submitForm(details){
    this.submitted = true;
    if (this.formreact.invalid) {
      return;
    }
    // else{
    //   // $("#exampleModal").modal("hide");
    //   alert('Sucess');
    //   location.reload(true);
    // }
    // console.log(id)
    // console.log(this.f.fname.value);
    // return false;
    const data = {
      
      id : details.id,
      fname: this.f.fname.value,
      mname: this.f.mname.value,
      lname: this.f.lname.value,
      bday: this.f.bday.value,
      gender: this.f.gender.value,
      password: this.f.password.value,
      contact: this.f.contact.value

    }
   
    this.infoservice.updateEmp(data)
    .subscribe(data => {
      location.reload(true);
      this.reloaddata();
      this.router.navigate['/infodetails']
      console.log(data);
    })
    // console.log(this.formreact.value)
 
  

// closing tag4 
}  

}