import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginserviceService } from 'src/app/service/loginservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  loginform: FormGroup;
  user = [];
  submitted = false; // submited deafault to false
  constructor(private _formbuilder: FormBuilder,
    private logservice: LoginserviceService,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit() {

    this.loginform = this._formbuilder.group({
      fname : ['', Validators.required ],
      password : ['', Validators.required ]
    });

  }

  get fname() { return this.loginform.get('fname'); }
  get password() { return this.loginform.get('password'); }

  get f() { return this.loginform.controls; }

  login(){

    this.submitted = true
    if (this.loginform.invalid) {
      return;
    }
    this.logservice.logservice(this.f.fname.value, this.f.password.value)
    .subscribe(data => {
      console.log(data);
      this.router.navigate(['/form']);
     },
     err => {console.log(err)
      const error: any = err;
      this.toastr.error("Invalid username and password")
      }
     );

  }



}


  // this.router.navigateByUrl('/form');
  //   else if(this.f.uname.value !== "Username"){
  //      this.toastr.error("Invalid username")
  //   }
  //   else if(this.f.password.value !== "password"){
  //     this.toastr.error("Invalid password")
  //  }
  //   else{
  //     this.router.navigate(['/form']);
  //   }