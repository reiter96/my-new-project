import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NewsServiceService } from 'src/app/service/news-service.service';

@Component({
  selector: 'app-news-add',
  templateUrl: './news-add.component.html',
  styleUrls: ['./news-add.component.css']
})
export class NewsAddComponent implements OnInit {
  newsform: FormGroup;
  submitted = false; // submited deafault to false
  constructor(private _formbuilder: FormBuilder, private newservice: NewsServiceService, private toastr: ToastrService ) { }l

  ngOnInit() {

    this.newsform = this._formbuilder.group({
      date : ['', Validators.required],
      news_no : ['', [Validators.required,Validators.pattern('^[0-9]+$')]],
      subject : ['', Validators.required],
      firstname : ['', [Validators.required,Validators.pattern('^[a-z A-Z]+$')]],
      middlename : ['', [Validators.required,Validators.pattern('^[a-z A-Z]+$')]],
      lastname : ['', [Validators.required,Validators.pattern('^[a-z A-Z]+$')]],

      no : ['', [Validators.required,Validators.pattern('^[0-9]+$')]],
      street : ['', Validators.required],
      barangay : ['', Validators.required],
      municipality : ['', Validators.required],
      province : ['', Validators.required],
      zipcode : ['', [Validators.required,Validators.pattern('^[0-9]+$')]],
      descrip : ['', Validators.required]
    });

  }

  get date() { return this.newsform.get('date'); }
  get news_no() { return this.newsform.get('news_no'); }
  get subject() { return this.newsform.get('subject'); }
  get firstname() { return this.newsform.get('firstname'); }
  get middlename() { return this.newsform.get('middlename'); }
  get lastname() { return this.newsform.get('lastname'); }

  get no() { return this.newsform.get('no'); }
  get street() { return this.newsform.get('street'); }
  get barangay() { return this.newsform.get('barangay'); }
  get municipality() { return this.newsform.get('municipality'); }
  get province() { return this.newsform.get('province'); }
  get zipcode() { return this.newsform.get('zipcode'); }
  get descrip() { return this.newsform.get('descrip'); }

  submitnews(){
    this.newservice.newsform(this.newsform.value)
    .subscribe(
      data => {// reset form
        this.newsform.reset();
        this.toastr.success("Successfully Added!")
        console.log('success', data)
      },
      error => console.error('Error', error)
    )
   
    console.log(this.newsform);
    // this.reactForm.reset();
  }

 } //
