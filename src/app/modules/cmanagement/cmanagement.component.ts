import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsertypeServiceService } from 'src/app/service/usertype-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cmanagement',
  templateUrl: './cmanagement.component.html',
  styleUrls: ['./cmanagement.component.css']
})
export class CmanagementComponent implements OnInit {

  cmform: FormGroup;
  constructor(private _formbuilder: FormBuilder,
              private typeuser: UsertypeServiceService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.cmform = this._formbuilder.group({
      utnumber : ['',  Validators.required],
      usertype : ['',  Validators.required],
      firstname : ['',  Validators.required],
      middlename : ['',  Validators.required],
      description : ['',  Validators.required]
    
  });
  }

  get utnumber() { return this.cmform.get('utnumber'); }
  get usertype() { return this.cmform.get('usertype'); }
  get firstname() { return this.cmform.get('firstname'); }
  get middlename() { return this.cmform.get('middlename'); }
  get description() { return this.cmform.get('description'); }

  submituser(){
    this.typeuser.create(this.cmform.value)
    .subscribe(
      data => {// reset form
        this.cmform.reset();
        this.toastr.success("Successfully Added!")
        console.log('success', data)
      },
      error => console.error('Error', error)
    )
}



}
