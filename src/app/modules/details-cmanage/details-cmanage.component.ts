import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CmanagementService } from 'src/app/interface/cmanagement-service';
import { UsertypeServiceService } from 'src/app/service/usertype-service.service';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-details-cmanage',
  templateUrl: './details-cmanage.component.html',
  styleUrls: ['./details-cmanage.component.css']
})
export class DetailsCmanageComponent implements OnInit {

  dataSource: MatTableDataSource<CmanagementService>;
  cdata: any = [];

  // initialization

  constructor(private cmservice: UsertypeServiceService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private  router: Router) { }

    
    // private toastr: ToastrService,
    // private  router: Router,

  ngOnInit() {
    this.showdata();
  }

  showdata(){
    this.cmservice.getdata().subscribe(data => { 
      this.cdata = data
      // this.dataSource = data.body;
      this.dataSource = new MatTableDataSource<CmanagementService>(this.cdata);
      this.dataSource.data = this.cdata;
    });
  }

  displayedColumns: string[] = ['utnumber', 'usertype', 'firstname', 'middlename', 'description', 'actions'];
  //
  delete(id: number){
    this.cmservice.deletedata(id).pipe(first()).subscribe(data => {
      console.log(data)
      this.showdata();
      this.router.navigate['/cmdetails']
      this.toastr.success("Successfully deleted")
    });
  }
}
