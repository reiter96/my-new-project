import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { BarsideComponent } from './components/barside/barside.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    BarsideComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
  ],
  exports:[
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    BarsideComponent
  ]
})
export class SharedModule { }
