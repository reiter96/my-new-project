import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarsideComponent } from './barside.component';

describe('BarsideComponent', () => {
  let component: BarsideComponent;
  let fixture: ComponentFixture<BarsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
