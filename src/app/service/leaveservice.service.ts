import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LeaveInterface } from '../interface/leave-interface';

@Injectable({
  providedIn: 'root'
})
export class LeaveserviceService {

  constructor(private http: HttpClient) { }
  
  url:string = "http://localhost:3000/leavedata";

  
  create(data) {
    return this.http.post<any>(this.url, data);
  }

  getleave(){
    return this.http.get<any>(this.url);
  }

  specificdata(id){
    return this.http.get(`${this.url}/${id}`) 
   }

  delete(id){
    return this.http.delete(`${this.url}/${id}`) 
   }

}
