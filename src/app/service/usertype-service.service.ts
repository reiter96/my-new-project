import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UsertypeServiceService {

  constructor(private http: HttpClient) { }

  url:string = "http://localhost:3000/user_type";

  
  create(data) {
    return this.http.post<any>(this.url, data);
  }
  getdata(){
    return this.http.get<any>(this.url);
  }

  specificdata(id){
    return this.http.get(`${this.url}/${id}`) 
   }
  deletedata(id){
    return this.http.delete(`${this.url}/${id}`)
   }

}
