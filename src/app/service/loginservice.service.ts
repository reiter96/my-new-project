import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { retry, catchError, shareReplay } from 'rxjs/operators'; ///handling errors
// import { of } from 'rxjs';
// import { do } from "rxjs/operators";
import { Observable } from "rxjs";
import { Userlog } from '../models/userlog';
import * as moment from 'moment';
 // import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {

  constructor(private http: HttpClient, private router: Router) { }

  url:string = "http://localhost:3000/usedataget/login";

 
  logservice(fname:string, password:string ) {
    return this.http.post<Userlog>(this.url, {fname, password})
    .pipe(shareReplay(1))

  }

  // // 

  //   private setSession(authResult) {
  //       const expiresAt = moment().add(authResult.expiresIn,'second');

  //       localStorage.setItem('id_token', authResult.idToken);
  //       localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
  //   }          

  //   logout() {
  //       localStorage.removeItem("id_token");
  //       localStorage.removeItem("expires_at");
  //   }

  //   public isLoggedIn() {
  //       return moment().isBefore(this.getExpiration());
  //   }

  //   isLoggedOut() {
  //       return !this.isLoggedIn();
  //   }

  //   getExpiration() {
  //       const expiration = localStorage.getItem("expires_at");
  //       const expiresAt = JSON.parse(expiration);
  //       return moment(expiresAt);
  //   }
  //   // 

  

}
