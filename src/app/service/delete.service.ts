import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeleteService {

  constructor(private http: HttpClient) { }

 private urls:string = "http://localhost:3000/usedatadelete";

  deleteEmployee(id: number){
    return this.http.delete(this.urls + '/' + id);
    // return this.http.delete(`${this.urls}/${id}`, { responseType: 'text' });
  }
 
  // deleteEmployee(userId): Observable<{}> {
  //   const deleteUrl = this.urls + '/' + userId;
  //   return this.http.delete(deleteUrl, this.httpOptions);
  // } 
  
}
