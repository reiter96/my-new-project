import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Userinterface } from '../interface/userinterface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsServiceService {
 
  constructor(private http: HttpClient) { }
  
 

  url = 'http://localhost:3000/newsdatapost';

  newsform(userData) {
    return this.http.post<any>(this.url, userData);
  }

  
  getdata(){
    return this.http.get<any>(this.url);
  }

  getspecific(id){
    return this.http.get(`${this.url}/${id}`) 
   }

  destroynews(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }

  updatenow(form: any){
    return this.http.patch(`${this.url}/${form.id}`,form);
  }

  

}
