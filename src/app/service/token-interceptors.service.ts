import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorsService implements HttpInterceptor{

  constructor(private injector: Injector) { }


  // intercept(req, next){
  //   const logservices = this.injector.get(LoginserviceService)
  //   const tokenizedReq = req.clone({
  //     setHeaders:{
  //       Authorization: `Bearer ${logservices.logservice()}`
  //     }
  //   });
  //   return next.handle(tokenizedReq)
  // }
  intercept(req: HttpRequest<any>,
            next: HttpHandler): Observable<HttpEvent<any>> {
             
            const token = localStorage.getItem("id_token");

            if (token){
              const clone = req.clone({
                headers: req.headers.set("Authorization", "Bearer" + token)
              });
              next.handle(clone);
              
            }else{
              return next.handle(req);
            }


  }


}
