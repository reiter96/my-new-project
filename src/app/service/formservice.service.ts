import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'



@Injectable({
  providedIn: 'root'
})
export class FormserviceService {

  constructor(private http: HttpClient) { }
  
 

  url = 'http://localhost:3000/usedatapost';
  createform(userData) {
    return this.http.post<any>(this.url, userData);
  }

}
