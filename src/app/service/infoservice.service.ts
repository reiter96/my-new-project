import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Userinterface } from '../interface/userinterface';
import { Form } from '../models/form';



@Injectable({
  providedIn: 'root'
})
export class InfoserviceService {

  constructor(private http: HttpClient) { }
  
  private url:string = "http://localhost:3000/usedataget";

  
  getdata(): Observable<Userinterface[]>{

    return this.http.get<Userinterface[]>(this.url);

  }
  //

  getspecific(getid){
    return this.http.get(`${this.url}/${getid}`) 
   }

   deleteEmployee(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }

  updateEmp(form: any){
    return this.http.patch(`${this.url}/${form.id}`,form);
  }

  



  

  
 
}
