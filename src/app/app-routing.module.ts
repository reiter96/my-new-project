import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layout/default/default.component';
import { HomeComponent } from './modules/home/home.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { FormComponent } from './modules/form/form.component';
import { InfodetailsComponent } from './modules/infodetails/infodetails.component';
import { SpecificinfoComponent } from './modules/specificinfo/specificinfo.component';
import { NewsAddComponent } from './modules/news-add/news-add.component';
import { AddImageComponent } from './modules/add-image/add-image.component';
import { LoginComponent } from './modules/login/login.component';
import { DetailsnewsComponent } from './modules/detailsnews/detailsnews.component';
import { CmanagementComponent } from './modules/cmanagement/cmanagement.component';
import { LeaveFormComponent } from './modules/leave-form/leave-form.component';
import { NewsspecificinfoComponent } from './modules/newsspecificinfo/newsspecificinfo.component';
import { LeavedetailsComponent } from './modules/leavedetails/leavedetails.component';
import { LeavespecificComponent } from './modules/leavespecific/leavespecific.component';
import { DetailsCmanageComponent } from './modules/details-cmanage/details-cmanage.component';
import { CmspecificinfoComponent } from './modules/cmspecificinfo/cmspecificinfo.component';



const routes: Routes = [
  {
    path:'home',
    component: DefaultComponent,
    children: [
    {
      path:'home',
      component: HomeComponent

    }
    
  ] //CHILDREN
  }, //first pwede maglagay
  {
    path:'dashboard',
    component: DashboardComponent
    
  },
  {
    path:'form',
    component: FormComponent
   


  },{
    path:'infodetails',
    component: InfodetailsComponent
    // ,canActivate: [GuardloginGuard]
  },{
    path:'infospecific/:id',
    component: SpecificinfoComponent
  },{
    path:'addnews',
    component: NewsAddComponent
  },{
    path:'add-image',
    component: AddImageComponent
  },{
    path:'login',
    component: LoginComponent
  },{
    path:'newsdetails',
    component: DetailsnewsComponent
  },{
    path:'cmanagement',
    component: CmanagementComponent
  },{
    path:'leaveform',
    component: LeaveFormComponent
  },{
    path:'newsspecificinfo/:id',
    component: NewsspecificinfoComponent
  },{
    path:'leavedetails',
    component: LeavedetailsComponent
  },{
    path:'leavespecific/:id',
    component: LeavespecificComponent
  },{
    path:'cmdetails',
    component: DetailsCmanageComponent
  },{
    path:'cmspecific/:id',
    component: CmspecificinfoComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot (routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }