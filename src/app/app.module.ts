import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultModule } from './layout/default/default.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TokenInterceptorsService } from './service/token-interceptors.service';
import { JwtModule } from "@auth0/angular-jwt";
import 'hammerjs';
// 
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import { MatNativeDateModule } from '@angular/material/core';
///
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
// 
import {MatDialogModule} from '@angular/material/dialog';
import { MydialogComponent } from './modules/mydialog/mydialog.component';
import { DetailsCmanageComponent } from './modules/details-cmanage/details-cmanage.component';
import { CmspecificinfoComponent } from './modules/cmspecificinfo/cmspecificinfo.component';
// 
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

export function tokenGetter() {
  return localStorage.getItem("access_token");
}


@NgModule({
  declarations: [
    AppComponent,
    MydialogComponent,
   
    
    
  ],
  imports: [
    NgbModule,
    //
    MatDialogModule,
    //
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    // 
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    // 
    BrowserModule,
    AppRoutingModule,
    DefaultModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(), 

    // // 
    // JwtModule.forRoot({
    //   config: {
    //     tokenGetter: tokenGetter,
    //     whitelistedDomains: ["example.com"],
    //     blacklistedRoutes: ["example.com/examplebadroute/"]
    //   }
    // })
    // 

  
  ],
  entryComponents: [
    MydialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


// 
// GuardloginGuard,
// {
//   provide: HTTP_INTERCEPTORS,
//   useClass: TokenInterceptorsService,
//   multi: true
// }
