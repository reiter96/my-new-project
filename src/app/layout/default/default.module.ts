import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import { HomeComponent } from 'src/app/modules/home/home.component';
import { DashboardComponent } from 'src/app/modules/dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormComponent } from 'src/app/modules/form/form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfodetailsComponent } from 'src/app/modules/infodetails/infodetails.component';
import { SpecificinfoComponent } from 'src/app/modules/specificinfo/specificinfo.component';
import { NewsAddComponent } from 'src/app/modules/news-add/news-add.component';
import { AddImageComponent } from 'src/app/modules/add-image/add-image.component';
import { LoginComponent } from 'src/app/modules/login/login.component';
import { DetailsnewsComponent } from 'src/app/modules/detailsnews/detailsnews.component';
import { CmanagementComponent } from 'src/app/modules/cmanagement/cmanagement.component';
import { LeaveFormComponent } from 'src/app/modules/leave-form/leave-form.component';
import { NewsspecificinfoComponent } from 'src/app/modules/newsspecificinfo/newsspecificinfo.component';
// 
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import { MatNativeDateModule } from '@angular/material/core';
// 
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import { LeavedetailsComponent } from 'src/app/modules/leavedetails/leavedetails.component';
import { LeavespecificComponent } from 'src/app/modules/leavespecific/leavespecific.component';
import {MatDialogModule} from '@angular/material/dialog';
import { DetailsCmanageComponent } from 'src/app/modules/details-cmanage/details-cmanage.component';
import { CmspecificinfoComponent } from 'src/app/modules/cmspecificinfo/cmspecificinfo.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    
    // 
    DefaultComponent,
    HomeComponent,
    DashboardComponent,
    FormComponent,
    InfodetailsComponent,
    SpecificinfoComponent,
    NewsAddComponent,
    AddImageComponent,
    LoginComponent,
    DetailsnewsComponent,
    CmanagementComponent,
    LeaveFormComponent,
    NewsspecificinfoComponent,
    LeavedetailsComponent,
    LeavespecificComponent,
    DetailsCmanageComponent,
    CmspecificinfoComponent
  ],
  imports: [
    NgbModule,
    // 
    MatDialogModule,
    //
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    // 
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    // MaterialModule,  
    // MatMomentDateModule,
    // 
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule  //this module is the shared where you can find the header, footer
  ]
 
})
export class DefaultModule { }
