export interface Userinterface {
    id: number,     
    fname : String,
    mname : String,
    lname : String,
    bday : String,
    gender : String,
    password : String, 
    contact : String,
    status : String
}
