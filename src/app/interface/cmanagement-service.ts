export interface CmanagementService {
    id: number;     
    utnumber : String;
    usertype : String;
    firstname : String;
    middlename : String;
    description : String;
}
